﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW3App1
{
	public partial class MainPage : ContentPage
	{
        public Entry stringEntry;
		public MainPage()
		{
			InitializeComponent();
            BackgroundImage = "icon.png";//image
            stringEntry = new Entry
            {
                Text = "Enter string here",
                FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                FontAttributes = FontAttributes.Bold
            };
            //stringLabel.SetBinding(Label.TextProperty, "passedString");

            var navigateButton = new Button { Text = "Next Page" };
            navigateButton.Clicked += OnNavigateButtonClicked;



            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                Children = {

                    new StackLayout {
                        Orientation = StackOrientation.Horizontal,
                        Children = {
                            new Label {
                                Text = "Passing string:",
                                FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
                                HorizontalOptions = LayoutOptions.FillAndExpand
                            },
                            stringEntry
                        }
                    },
                    navigateButton
                }
            };
        }

        async void OnNavigateButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1(stringEntry.Text));
        }
    }
}
